package com.vlover.dao;

public class UserDAOFactory {
	public static UserDAO getUserDAONewInstance() {
		System.out.println("通过工厂类取得代理类的实例去调用代理类的操作。");
		return new UserDAOProxy();
	}
}