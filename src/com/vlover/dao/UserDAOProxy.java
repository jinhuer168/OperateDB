package com.vlover.dao;

public class UserDAOProxy implements UserDAO {

	private UserDAOImpl dao = null;
	private String str = null; // 此处其实应该传入一个数据库的连接到真实主题类的，不过这里只是个演示，所有以字符串的形式传了进去

	public UserDAOProxy() {
		this.str = new String();
		this.dao = new UserDAOImpl(this.str);
	}

	public boolean doCreate(User user) throws Exception {
		boolean flag = true;
		System.out.println("代理类的操作，打开数据库，同时取得真实主题类的实例去调用真实的数据层操作");
		try {
			this.dao.doCreate(user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("代理类的操作，调用完真实主题类的操作，同时关闭数据库");
		}
		return flag;
	}

}
