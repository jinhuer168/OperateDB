package com.vlover.dbfactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public class FactoryDemo {
	public static void main(String args[]) throws Exception {
		List<Student> students = MysqlDAOFactory.getUserDAONewInstance("mysql").query("select * from student");
		for (Student s : students) {
			System.out.println(s.getName() + " : " + s.getAge());
		}
	}
}
