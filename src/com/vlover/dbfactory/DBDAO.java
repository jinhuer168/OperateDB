package com.vlover.dbfactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public interface DBDAO {
	public Connection getConnection();
	
	public void closeAllConnection(ResultSet rs, Statement stat, Connection conn);
	
	public int update(String sql, String... pras);
	
	public List<Student> query(String sql, String... pras);
	
}