package com.vlover.dbfactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MysqlDAOImpl implements DBDAO {

	/**
	 * 数据库驱动对象
	 */
	public static final String DRIVER = "com.mysql.jdbc.Driver";

	/**
	 * 数据库连接地址(数据库名)
	 */
	public static final String URL = "jdbc:mysql://localhost:3306/gqz";

	/**
	 * 登陆名
	 */
	public static final String USER = "root";

	/**
	 * 登陆密码
	 */
	public static final String PWD = "";

	/**
	 * 建立连接
	 */
	@Override
	public Connection getConnection() {
		Connection conn = null;

		try {
			Class.forName(DRIVER);// 加载Oracle驱动程序

			System.out.println("开始尝试连接数据库！");

			conn = DriverManager.getConnection(URL, USER, PWD);// 获取连接
			System.out.println("Mysql数据库连接成功！");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("Null pointer");
		}
		return conn;
	}

	/**
	 * 关闭连接
	 */
	@Override
	public void closeAllConnection(ResultSet rs, Statement stat, Connection conn) {
		try {
			if (null != rs) {
				rs.close();
				rs = null;
			}

			if (null != stat) {
				stat.close();
				stat = null;
			}

			if (null != conn) {
				conn.close();
				conn = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param sql数据库更新
	 *            (增、删、改) 语句
	 * @param pras参数列表
	 *            （可传，可不传，不传为NULL，以数组形式存在）
	 * @return 返回受影响都行数
	 */
	public int update(String sql, String... pras) {
		int resu = 0;
		Connection conn = this.getConnection();
		PreparedStatement pstat = null;
		try {
			pstat = conn.prepareStatement(sql);
			for (int i = 0; i < pras.length; i++) {
				pstat.setString(i + 1, pras[i]);
			}
			resu = pstat.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeAllConnection(null, pstat, conn);
			;
		}
		return resu;
	}

	/**
	 * 查询数据库内容，并返回结果（这里的resultset一定不要关闭，否则后面无法查询内容）
	 * 
	 * @param sql数据库查询语句
	 * @param pras参数列表
	 *            （可传，可不传，不传为NULL，以数组形式存在）
	 * @return 返回结果集
	 */
	public List<Student> query(String sql, String... pras) {

		List<Student> students = new ArrayList<>();

		Connection conn = this.getConnection();
		PreparedStatement pstat = null;
		ResultSet rs = null;
		try {
			pstat = conn.prepareStatement(sql);

			if (pras != null)
				for (int i = 0; i < pras.length; i++) {
					pstat.setString(i + 1, pras[i]);
				}
			rs = pstat.executeQuery();
			// print(rs);
			students = getStudents(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAllConnection(rs, pstat, conn);
		}

		return students;
	}

	private List<Student> getStudents(ResultSet rs) throws SQLException {

		List<Student> students = new ArrayList<>();
		Student student = null;

		while (rs.next()) {
			student = new Student();
			student.setAge(rs.getInt("age"));
			student.setName(rs.getString("name"));

			students.add(student);
		}
		return students;
	}

	/**
	 * 打印查询内容
	 * 
	 * @param rs
	 */
	public void print(ResultSet rs) {
		try {
			ResultSetMetaData columns = rs.getMetaData();
			for (int i = 1; i <= columns.getColumnCount(); i++) {
				System.out.print(columns.getColumnName(i));
				System.out.print("\t\t");
			}
			System.out.println();

			while (rs.next()) {
				for (int i = 1; i <= columns.getColumnCount(); i++) {
					System.out.print(rs.getString(i));
					System.out.print("\t\t");
				}
				System.out.println();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
