package com.vlover.dbfactory;

public class MysqlDAOFactory {
	public static DBDAO getUserDAONewInstance(String str) {
		if (str.equals("mysql".toLowerCase())) {
			return new MysqlDAOImpl();
		} else if (str.equals("oracle".toLowerCase())) {
			return new OracleDAOImpl();
		} else {
			return null;
		}
	}
}