package com.vlover.DBConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

abstract class ConnDB {
		
	/**
	 * 创建一个数据库连接
	 */
	private Connection conn = null;

	/**
	 * 创建预编译语句对象，一般都是用这个而不用Statement
	 */
	private PreparedStatement ps = null;

	/**
	 * 创建一个结果集对象
	 */
	private ResultSet rs = null;
	
	/**
	 * 一个非常标准的连接Oracle数据库的示例代码
	 * 
	 * @return
	 */
	public abstract Connection getConn() ;
	
	/**
	 * 关闭数据库连接
	 */
	public void closeAll() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("数据库关闭成功。");
	}
	

	/**
	 * @param sql数据库更新
	 *            (增、删、改) 语句
	 * @param pras参数列表
	 *            （可传，可不传，不传为NULL，以数组形式存在）
	 * @return 返回受影响都行数
	 */
	public int update(String sql, String... pras) {
		int resu = 0;
		conn = getConn();
		try {
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < pras.length; i++) {
				ps.setString(i + 1, pras[i]);
			}
			resu = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll();
		}
		return resu;
	}

	/**
	 * @param sql数据库查询语句
	 * @param pras参数列表
	 *            （可传，可不传，不传为NULL，以数组形式存在）
	 * @return 返回结果集
	 */
	public ResultSet query(String sql, String... pras) {
		conn = getConn();
		try {
			ps = conn.prepareStatement(sql);

			if (pras != null)
				for (int i = 0; i < pras.length; i++) {
					ps.setString(i + 1, pras[i]);
				}
			rs = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		print(rs);
		return rs;
	}
	
	/**
	 * 打印查询内容
	 * @param rs
	 */
	public void print(ResultSet rs) {
		try {
			ResultSetMetaData columns = rs.getMetaData();
			for (int i = 1; i <= columns.getColumnCount(); i++) {
				System.out.print(columns.getColumnName(i));
				System.out.print("\t\t");
			}
			System.out.println();
			
			while (rs.next()) {
				for (int i = 1; i <= columns.getColumnCount(); i++) {
					System.out.print(rs.getString(i));
					System.out.print("\t\t");
				}
				System.out.println();
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
