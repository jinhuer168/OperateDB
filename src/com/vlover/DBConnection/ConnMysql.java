package com.vlover.DBConnection;

import java.awt.datatransfer.StringSelection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ConnMysql extends ConnDB {

	/**
	 * 数据库驱动对象
	 */
	public static final String DRIVER = "com.mysql.jdbc.Driver";

	/**
	 * 数据库连接地址(数据库名)
	 */
	public static final String URL = "jdbc:mysql://localhost:3306/gqz";

	/**
	 * 登陆名
	 */
	public static final String USER = "root";

	/**
	 * 登陆密码
	 */
	public static final String PWD = "";

	@Override
	public Connection getConn() {
		Connection conn = null;

		try {
			Class.forName(DRIVER);// 加载Oracle驱动程序

			System.out.println("开始尝试连接数据库！");

			conn = DriverManager.getConnection(URL, USER, PWD);// 获取连接
			System.out.println("Mysql数据库连接成功！");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return conn;
	}

}
