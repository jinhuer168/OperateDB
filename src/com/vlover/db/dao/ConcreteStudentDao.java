package com.vlover.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class ConcreteStudentDao implements StudentDAO {

	// 增加一个学生
	public int addStudent(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		int i = 0;
		try {
			con = JDBCUtils.getConnection();
			String sql = "insert into student(name,age) values(?,?)";
			ps = con.prepareStatement(sql);

			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());

			i = ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e.getMessage(), e);
		} finally {
			JDBCUtils.free(null, ps, con);
		}
		return i;
	}

	// 删除一个学生
	public int deleteStudent(String name) {
		Connection con = null;
		PreparedStatement ps = null;
		int i = 0;
		try {
			con = JDBCUtils.getConnection();
			String sql = "delete from student where name =?";
			ps = con.prepareStatement(sql);
			ps.setString(1, name);

			i = ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e.getMessage(), e);
		} finally {
			JDBCUtils.free(null, ps, con);
		}

		return i;
	}

	// 修改一个学生
	public int updateStudent(String name) {
		Connection con = null;
		PreparedStatement ps = null;
		int i = 0;
		try {
			con = JDBCUtils.getConnection();
			String sql = "update student set age=age+1  where name =?";
			ps = con.prepareStatement(sql);
			ps.setString(1, name);

			i = ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e.getMessage(), e);
		} finally {
			JDBCUtils.free(null, ps, con);
		}

		return i;
	}

	// 查询一行
	public Student findStudent(String name) {
		Connection con = null;
		PreparedStatement ps = null;
		Student stu = null;
		ResultSet rs = null;
		try {
			con = JDBCUtils.getConnection();
			String sql = "select name,age from student where name =?";
			ps = con.prepareStatement(sql);
			ps.setString(1, name);

			rs = ps.executeQuery();
			stu = new Student();
			while (rs.next()) {
				stu.setName(rs.getString(1));
				stu.setAge(rs.getInt(2));

			}

		} catch (SQLException e) {
			throw new DAOException(e.getMessage(), e);
		} finally {
			JDBCUtils.free(rs, ps, con);
		}

		return stu;
	}

	// 查询所有
	public Set<Student> findAll() {
		Connection con = null;
		PreparedStatement ps = null;
		Student stu = null;
		ResultSet rs = null;
		Set<Student> set = null;
		try {
			con = JDBCUtils.getConnection();
			String sql = "select name,age  from student";
			ps = con.prepareStatement(sql);

			set = new HashSet<Student>();
			rs = ps.executeQuery();

			while (rs.next()) {
				stu = new Student();

				stu.setName(rs.getString(1));
				stu.setAge(rs.getInt(2));

				set.add(stu);
			}

		} catch (SQLException e) {
			throw new DAOException(e.getMessage(), e);
		} finally {
			JDBCUtils.free(rs, ps, con);
		}

		return set;
	}

}
