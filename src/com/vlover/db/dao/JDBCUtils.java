package com.vlover.db.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCUtils {
	/**
	 * 获取连接
	 * 
	 */
	public static Connection getConnection() {
		String driverName = "com.mysql.jdbc.Driver";

		String url = "jdbc:mysql://localhost:3306/gqz";
		String user = "root";
		String password = "";
		Connection con = null;
		try {

			Class.forName(driverName);
			con = DriverManager.getConnection(url, user, password);
			System.out.println("MySQL 连接成功。");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return con;

	}

	/**
	 * 关闭连接
	 */
	public static void free(ResultSet rs, Statement sta, Connection con) {
		try {
			if (null != rs) {
				rs.close();
				rs = null;
			}

			if (null != sta) {
				sta.close();
				sta = null;
			}

			if (null != con) {
				con.close();
				con = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
