package com.vlover.db.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class Client {
   public static void main(String[] args)
   {
	   Student stu = new Student();
	   Set<Student> set = new HashSet<Student>();

	   StudentService ss = new StudentService();
	   set = ss.findAll() ;
	   Iterator<Student> iterator = set.iterator();
	   while(iterator.hasNext())
	   {
		  Student student =  (Student)iterator.next() ;
		  System.out.println(student.getName() +" " +student.getAge());
	   }
   }
}
