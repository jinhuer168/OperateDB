use gqz;

create database gqz;

create user gqz identified by 'gqz';

grant create,select,insert,delete on gqz.* to gqz;

CREATE TABLE EMP
(
    EMPNO INT(4) NOT NULL,
    ENAME VARCHAR(10),
    JOB VARCHAR(9),
    MGR INT(4),
    HIREDATE DATE,
    SAL DECIMAL(7, 2),
    COMM DECIMAL(7, 2),
    DEPTNO INT(2)
);

INSERT INTO EMP VALUES (7369, 'SMITH',  'CLERK',     7902,   str_to_date('17-12-1980', '%d-%m-%Y'),  800, NULL, 20);
INSERT INTO EMP VALUES (7499, 'ALLEN',  'SALESMAN',  7698,   str_to_date('20-2-1981', '%d-%m-%Y'), 1600,  300, 30);
INSERT INTO EMP VALUES (7521, 'WARD',   'SALESMAN',  7698,   str_to_date('22-2-1981', '%d-%m-%Y'), 1250,  500, 30);
INSERT INTO EMP VALUES (7566, 'JONES',  'MANAGER',   7839,   str_to_date('2-4-1981',  '%d-%m-%Y'),  2975, NULL, 20);
INSERT INTO EMP VALUES (7654, 'MARTIN', 'SALESMAN',  7698,   str_to_date('28-9-1981', '%d-%m-%Y'), 1250, 1400, 30);
INSERT INTO EMP VALUES (7698, 'BLAKE',  'MANAGER',   7839,   str_to_date('1-5-1981',  '%d-%m-%Y'),  2850, NULL, 30);
INSERT INTO EMP VALUES (7782, 'CLARK',  'MANAGER',   7839,   str_to_date('9-6-1981',  '%d-%m-%Y'),  2450, NULL, 10);
INSERT INTO EMP VALUES (7788, 'SCOTT',  'ANALYST',   7566,   str_to_date('09-12-1982', '%d-%m-%Y'), 3000, NULL, 20);
INSERT INTO EMP VALUES (7839, 'KING',   'PRESIDENT', NULL,   str_to_date('17-11-1981', '%d-%m-%Y'), 5000, NULL, 10);
INSERT INTO EMP VALUES (7844, 'TURNER', 'SALESMAN',  7698,   str_to_date('8-9-1981',  '%d-%m-%Y'),  1500,    0, 30);
INSERT INTO EMP VALUES (7876, 'ADAMS',  'CLERK',     7788,   str_to_date('12-1-1983', '%d-%m-%Y'), 1100, NULL, 20);
INSERT INTO EMP VALUES (7900, 'JAMES',  'CLERK',     7698,   str_to_date('3-12-1981',  '%d-%m-%Y'),   950, NULL, 30);
INSERT INTO EMP VALUES (7902, 'FORD',   'ANALYST',   7566,   str_to_date('3-12-1981',  '%d-%m-%Y'),  3000, NULL, 20);
INSERT INTO EMP VALUES (7934, 'MILLER', 'CLERK',     7782,   str_to_date('23-1-1982', '%d-%m-%Y'), 1300, NULL, 10);

CREATE TABLE DEPT
(
    DEPTNO INT(2),
    DNAME VARCHAR(14),
    LOC VARCHAR(13)
);

INSERT INTO DEPT VALUES (10, 'ACCOUNTING', 'NEW YORK');
INSERT INTO DEPT VALUES (20, 'RESEARCH',   'DALLAS');
INSERT INTO DEPT VALUES (30, 'SALES',      'CHICAGO');
INSERT INTO DEPT VALUES (40, 'OPERATIONS', 'BOSTON');

alter table EMP add constraint emp_pk primary key(EMPNO);
alter table DEPT add constraint dept_pk primary key(DEPTNO);
alter table EMP add constraint emp_fk_dept foreign key(DEPTNO) references dept(DEPTNO);
alter table EMP add constraint emp_fk_emp foreign key(MGR) references emp(EMPNO);
